import 'package:alcodes_on_board_flutter/models/response_models/friend_list_response_model.dart';
import 'package:alcodes_on_board_flutter/repository/friend_list_repo.dart';
import 'package:flutter/material.dart';

class MyFriendDetail extends StatefulWidget {
  Map arguments;

  @override
  _MyFriendDetailState createState() => _MyFriendDetailState();

  MyFriendDetail({
    Key key,
    this.arguments,
  }) : super(key: key);
}

class _MyFriendDetailState extends State<MyFriendDetail> {
  Future<FriendListResponseModel> _futureBuilder;
  FriendListResponseModel _friend;

  Future<FriendListResponseModel> getFriendAsync() async {
    _friend =
        await FriendListRepo().requestSingleFriendAsync(widget.arguments['id']);
    return _friend;
  }

  @override
  void initState() {
    _futureBuilder = getFriendAsync();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        title: Text('My Friend Detail'),
      ),
      body: FutureBuilder(
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasData) {
            return _content();
          }
          Future(() {
            showDialog(
              context: context,
              barrierDismissible: false,
              builder: (context) {
                return AlertDialog(
                  scrollable: true,
                  title: Text("Error"),
                  content: Text('Network Error!'),
                  actions: [
                    FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('Ok'),
                    ),
                  ],
                );
              },
            );
          });

          return Center(
            child: Text('Error'),
          );
        },
        future: _futureBuilder,
      ),
    );
  }

  Widget _content() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            new Container(
                width: 190.0,
                height: 190.0,
                decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                    image: new DecorationImage(
                        fit: BoxFit.contain,
                        image: new NetworkImage(_friend.avatar)))),
            SizedBox(
              height: 15,
            ),
            Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  color: Colors.white,
                ),
                padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text('First Name', style: TextStyle(fontSize: 20.0)),
                    SizedBox(
                      height: 5,
                    ),
                    Text(_friend.firstName,
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[600])),
                  ],
                )),
            SizedBox(
              height: 15,
            ),
            Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  color: Colors.white,
                ),
                padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text('Last Name', style: TextStyle(fontSize: 20.0)),
                    SizedBox(
                      height: 5,
                    ),
                    Text(_friend.lastName,
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[600])),
                  ],
                )),
            SizedBox(
              height: 15,
            ),
            Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  color: Colors.white,
                ),
                padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text('Email', style: TextStyle(fontSize: 20.0)),
                    SizedBox(
                      height: 5,
                    ),
                    Text(_friend.email,
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[600])),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}

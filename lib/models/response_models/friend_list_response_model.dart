class FriendListResponseModel {
  int id;
  String firstName;
  String lastName;
  String avatar;
  String email;

  FriendListResponseModel(
      {this.id, this.firstName, this.lastName, this.avatar, this.email});

  factory FriendListResponseModel.fromJson(dynamic json) {
    return FriendListResponseModel(
        id: json['id'] as int,
        firstName: json['first_name'] as String,
        lastName: json['last_name'] as String,
        avatar: json['avatar'] as String,
        email: json['email'] as String);
  }
}

import 'dart:convert';

import 'package:alcodes_on_board_flutter/constants/api_urls.dart';
import 'package:alcodes_on_board_flutter/models/response_models/friend_list_response_model.dart';
import 'package:alcodes_on_board_flutter/utils/api_components/api_response.dart';
import 'package:dio/dio.dart';

class FriendListRepo {
  Future<ApiResponse<List<FriendListResponseModel>>>
      requestFriendListAsync() async {
    try {
      Response response = await Dio().get(ApiUrls.baseUrl + 'users?page=1');
      List data = jsonDecode(response.toString())['data'] as List;
      List<FriendListResponseModel> lists =
          data.map((item) => FriendListResponseModel.fromJson(item)).toList();
      return ApiResponse<List<FriendListResponseModel>>(
          message: 'OK', data: lists);
    } on DioError catch (ex) {
      if (ex.response.statusCode == 400) {
        return Future.error(ex.response.data['error']);
      } else {
        return Future.error(ex);
      }
    } catch (ex) {
      return Future.error(ex);
    }
  }

  Future<FriendListResponseModel> requestSingleFriendAsync(int id) async {
    try {
      Response response =
          await Dio().get(ApiUrls.baseUrl + 'users/' + id.toString());
      Map data = jsonDecode(response.toString())['data'];
      FriendListResponseModel friend = FriendListResponseModel.fromJson(data);
      return friend;
    } on DioError catch (ex) {
      if (ex.response.statusCode == 400) {
        return Future.error(ex.response.data['error']);
      } else {
        return Future.error(ex);
      }
    } catch (ex) {
      return Future.error(ex);
    }
  }
}
